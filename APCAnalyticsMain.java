import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.json.simple.JSONArray;

import jsonhandling.JSONExtract;
import util.DBUtil;
import util.Utility;
import vehActivity.APCActivity;

public class APCAnalyticsMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("/home/ubuntu/apc/config.properties");
			//input = new FileInputStream("/home/guru/eclipse-workspace/APCAnalyticsNew/src/config.properties");
			
			// load properties file
			prop.load(input);
			 
			//Load results from the JSON query
			String URL_Query = prop.getProperty("URL_Query");
			JSONExtract jsonExtract = new JSONExtract();
			JSONArray jsonarrVehicleActivity= jsonExtract.getjsonVehicleActivity(URL_Query);
			
			ArrayList<APCActivity> vehicleActivity = jsonExtract.getvehicleActivity(jsonarrVehicleActivity);
			
			
			//Load results from the APC Prod API
			String VEH_URL_Query = prop.getProperty("VEH_URL_Query");
			List<String> vehicleIdsFromFile = jsonExtract.getVehicleList(VEH_URL_Query);
			
			//ArrayList<APCActivity> vehicleActivity = jsonExtract.getvehicleActivity(jsonarrVehicleActivity);
			
		  
			//Get Matched records from file and query results
			Utility utility = new Utility();
			List<APCActivity> listOutput = utility.getMatchedItems(vehicleActivity, vehicleIdsFromFile);
			
			//Load Database properties
			String jdbcDriver = prop.getProperty("JDBC_DRIVER");
			String jdbcUrl = prop.getProperty("JDBC_URL");
			String username = prop.getProperty("JDBC_UNAME");
			String password = prop.getProperty("JDBC_PWD");
			
			//Save results to the database
			DBUtil dbUtil = new DBUtil();
			dbUtil.saveRecords(listOutput, jdbcDriver, jdbcUrl, username, password);
			
			
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		
	}

}
	}
}


package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import vehActivity.APCActivity;

public class DBUtil {
    private static final String INSERT_SQL = "INSERT INTO siri_dev_records "
    		+ "(vehicle_id , occupancy , progress_rate, route , block_ref , recorded_time,"
    		+ " latitude, longitude, stop_id, direction_ref, destination_name )VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    private static final String SELECT_SQL = "SELECT * FROM siri_dev_records "
    		+ " WHERE vehicle_id = ?  and recorded_time = ?";
    		
  

    public void saveRecords(List<APCActivity> listOutput, String jdbcDriver, String jdbcUrl, String username, String password) {
        {
            try {
                Class.forName(jdbcDriver);
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
          
			try {
			    Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
			    try {
			        conn.setAutoCommit(false);
					
					try { 
						PreparedStatement stmt1 = conn.prepareStatement(SELECT_SQL);
					    PreparedStatement stmt = conn.prepareStatement(INSERT_SQL);
					    ResultSet rs = null;
					    try {
					        int counter = 0;
					        int i = 0;
					        while (i < listOutput.size()) {
					        	int s = Integer.parseInt(listOutput.get(i).getVehId());
					        	stmt1.setInt(1, Integer.parseInt(listOutput.get(i).getVehId()));
					        	stmt1.setTimestamp(2, listOutput.get(i).getRecordedAtTime());					        	
					        	rs = stmt1.executeQuery();
					        	boolean flag = rs.next();					        	
					        	
					        	if(!flag) {
					        	stmt.setInt(1, Integer.parseInt(listOutput.get(i).getVehId()));
					            stmt.setString(2, listOutput.get(i).getOccupancy());
					            stmt.setString(3, listOutput.get(i).getProgressRate());
					            stmt.setString(4, listOutput.get(i).getRoute());
					            stmt.setString(5, listOutput.get(i).getBlockRef());
					            stmt.setTimestamp(6, listOutput.get(i).getRecordedAtTime());
					            stmt.setDouble(7, listOutput.get(i).getLatitude());
					            stmt.setDouble(8, listOutput.get(i).getLongitude());
					            stmt.setString(9, listOutput.get(i).getStopPointRef());
					            stmt.setInt(10,Integer.parseInt(listOutput.get(i).getDirectionRef()));
					            stmt.setString(11, listOutput.get(i).getDestinationName());
					            stmt.executeUpdate();
					            conn.commit();
					            counter++;
					        	}++i;
					        }
					        System.out.println("Inserted "+ counter +"rows successfully");
					    }
					    
					    finally {
					    	if (rs != null)
					    		rs.close();
					        if (stmt != null) {
					            stmt.close();
					        }if (stmt1 != null) {
					            stmt1.close();
					        }
					    }
					
					
			    }
			    finally {
			        if (conn != null) {
			            conn.close();
			        }
			    }
			}
			catch (SQLException e1) {
				 e1.printStackTrace();
			}
         }
         catch (SQLException e1) {
			e1.printStackTrace();
         }
    }
}
}
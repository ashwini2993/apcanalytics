package util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import vehActivity.APCActivity;

public class Utility {
    public List<APCActivity> getMatchedItems(ArrayList<APCActivity> vehicleActivity, List<String> vehicleIdsFromFile) {
        List<APCActivity> listOutput = vehicleActivity.stream().filter(e -> vehicleIdsFromFile.contains(e.getVehId())).collect(Collectors.toList());
       
        System.out.println("From JSON Query " + vehicleActivity.size() + " From file " + vehicleIdsFromFile.size() + " Filtered size " + listOutput.size());
        return listOutput;
    }
}
package vehActivity;

import java.sql.Timestamp;

public class APCActivity {
  private String vehId;
  private String occupancy;
  private String progressRate;
  private String route;
  private String blockRef;
  private String stopPointRef;
  private double latitude;
  private double longitude;
  private Timestamp recordedAtTime;
  private String directionRef;
  private String destinationName;
  
 
public String getDirectionRef() {
	return directionRef;
}

public void setDirectionRef(String directionRef) {
	this.directionRef = directionRef;
}

public String getDestinationName() {
	return destinationName;
}

public void setDestinationName(String destinationName) {
	this.destinationName = destinationName;
}

public APCActivity() {}

public String getVehId() {
	return vehId;
}

public void setVehId(String vehId) {
	this.vehId = vehId;
}

public String getOccupancy() {
	return occupancy;
}

public void setOccupancy(String occupancy) {
	this.occupancy = occupancy;
}

public String getProgressRate() {
	return progressRate;
}

public void setProgressRate(String progressRate) {
	this.progressRate = progressRate;
}

public String getRoute() {
	return route;
}

public void setRoute(String route) {
	this.route = route;
}

public String getBlockRef() {
	return blockRef;
}

public void setBlockRef(String blockRef) {
	this.blockRef = blockRef;
}

public String getStopPointRef() {
	return stopPointRef;
}

public void setStopPointRef(String stopPointRef) {
	this.stopPointRef = stopPointRef;
}

public double getLatitude() {
	return latitude;
}

public void setLatitude(double latitude) {
	this.latitude = latitude;
}

public double getLongitude() {
	return longitude;
}

public void setLongitude(double longitude) {
	this.longitude = longitude;
}

public Timestamp getRecordedAtTime() {
	return recordedAtTime;
}

public void setRecordedAtTime(Timestamp recordedAtTime) {
	this.recordedAtTime = recordedAtTime;
}
  
  

 
 
}


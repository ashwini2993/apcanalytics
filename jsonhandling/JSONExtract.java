package jsonhandling;

import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import vehActivity.APCActivity;

public class JSONExtract {
	
	public String getjsonResponse(String URL_Query) {
        String inline = "";
        
        try {
            URL url = new URL(URL_Query);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responsecode = conn.getResponseCode();
            System.out.println("Response code is: " + responsecode);
            if (responsecode != 200) {
                throw new RuntimeException("HttpResponseCode: " + responsecode);
            }
            Scanner sc = new Scanner(url.openStream());
            while (sc.hasNext()) {
                inline = String.valueOf(inline) + sc.nextLine();
            }
            System.out.println("\nJSON Response in String format");
            sc.close();
            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return inline;
	}
	
    public JSONArray getjsonVehicleActivity(String URL_Query) {
        String inline = "";
        JSONArray jsonarrVehicleActivity = null;
        try {
            inline = getjsonResponse(URL_Query);
            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject)parse.parse(inline);
            JSONObject jobjSiri = (JSONObject)jobj.get((Object)"Siri");
            JSONObject jobjServiceDel = (JSONObject)jobjSiri.get((Object)"ServiceDelivery");
            JSONArray jsonarrVehicleMonDel = (JSONArray)jobjServiceDel.get((Object)"VehicleMonitoringDelivery");
            JSONObject jobjVehMonitorDel = (JSONObject)jsonarrVehicleMonDel.get(0);
            jsonarrVehicleActivity = (JSONArray)jobjVehMonitorDel.get((Object)"VehicleActivity");
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return jsonarrVehicleActivity;
    }

    public ArrayList<APCActivity> getvehicleActivity(JSONArray jsonarrVehicleActivity) {
        ArrayList<APCActivity> vehicleActivity = new ArrayList<APCActivity>();
        int i = 0;
        while (i < jsonarrVehicleActivity.size()) {
            JSONObject jsonVehicleActivity = (JSONObject)jsonarrVehicleActivity.get(i);
            JSONObject monitoredVehicleJourney = (JSONObject)jsonVehicleActivity.get((Object)"MonitoredVehicleJourney");
            APCActivity apcActivityMon = new APCActivity();
            String vehIdString = (String)monitoredVehicleJourney.get((Object)"VehicleRef");
            String occupancy = null;
            occupancy = (String)monitoredVehicleJourney.get((Object)"Occupancy");
            String progressRate = (String)monitoredVehicleJourney.get((Object)"ProgressRate");
            String routeString = (String)monitoredVehicleJourney.get((Object)"LineRef");
            String blockRef = (String)monitoredVehicleJourney.get((Object)"BlockRef");
            String directionRef = (String)monitoredVehicleJourney.get((Object)"DirectionRef");
            String destinationName = (String)monitoredVehicleJourney.get((Object)"DestinationName");
            
            JSONObject jsonVehicleMonitoredCall = (JSONObject)monitoredVehicleJourney.get((Object)"MonitoredCall");
            String stopPointRef ="";
            if(jsonVehicleMonitoredCall!=null)
            	stopPointRef = (String)jsonVehicleMonitoredCall.get((Object)"StopPointRef");
           
            JSONObject jsonVehicleLocation = (JSONObject)monitoredVehicleJourney.get((Object)"VehicleLocation");
            double latitude = (Double)jsonVehicleLocation.get((Object)"Latitude");
            double longitude = (Double)jsonVehicleLocation.get((Object)"Longitude");
            String recordedAtTimeStr = (String)jsonVehicleActivity.get((Object)"RecordedAtTime");
           
            String pattern = "yyyy-MM-dd'T'HH:mm:ss";
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            Date recordedAtDate = null;
            try {
                recordedAtDate = sdf.parse(recordedAtTimeStr);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            Timestamp recordedAtTime = new Timestamp(recordedAtDate.getTime());
            String vehId = vehIdString.substring(vehIdString.lastIndexOf(95) + 1);
            apcActivityMon.setVehId(vehId);
            apcActivityMon.setOccupancy(occupancy);
            apcActivityMon.setProgressRate(progressRate);
            String route = routeString.substring(routeString.lastIndexOf(95) + 1);
            apcActivityMon.setRoute(route);
            apcActivityMon.setBlockRef(blockRef);
            apcActivityMon.setRecordedAtTime(recordedAtTime);
            apcActivityMon.setLatitude(latitude);
            apcActivityMon.setLongitude(longitude);
            apcActivityMon.setStopPointRef(stopPointRef);
            apcActivityMon.setDirectionRef(directionRef);
            apcActivityMon.setDestinationName(destinationName);
            vehicleActivity.add(apcActivityMon);
            ++i;
        }
        return vehicleActivity;
    }
    
    
    public ArrayList<String> getVehicleList(String URL_Query) {
        String inline = "";
        ArrayList<String> vehicleList = new ArrayList<String>();
        try {
           inline = getjsonResponse(URL_Query);
            JSONParser parse = new JSONParser();
            JSONArray jsonVehListArr = (JSONArray)parse.parse(inline);
            int i=0;
           
            while (i < jsonVehListArr.size()) {
                JSONObject jsonVehicleListObj = (JSONObject)jsonVehListArr.get(i);
                String systemName = (String)jsonVehicleListObj.get((Object)"systemName");
                String vehicleId = (String)jsonVehicleListObj.get((Object)"vehicleNumber");
                if(systemName.contains("APCR1"))
                	vehicleList.add(vehicleId);
                	
                i++;
                
            }
       
         
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return vehicleList;
    }

}